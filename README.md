# Dog Application

Welcome to the DogHouse Application - a simple RESTful service to manage dog data. This application is built with Quarkus.

## Prerequisites

- JDK 17
- Apache Maven 3.9.5

Ensure Java and Maven are correctly installed on your system by running:
```sh
java -version
mvn --version
```
Building the Application
To compile the application and package it into a JAR file, run the following command in the root directory of the project:

```sh
mvn clean package
```

Running the Application
```sh
mvn quarkus:dev
```

This will start the application on http://localhost:8080.

use PostmanCollection from Project, you can find it in resources folder