import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jakarta.json.bind.JsonbException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.StaleObjectStateException;


@Provider
public class RestExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception exception) {
        if (hasExceptionInChain(exception, ObjectNotFoundException.class)) {
            return Response.status(Response.Status.NOT_FOUND).entity(exception.getMessage()).build();
        }
        if (hasExceptionInChain(exception, StaleObjectStateException.class)) {
            return Response.status(Response.Status.CONFLICT).build();
        }
        if (hasExceptionInChain(exception, JsonbException.class) && exception.getMessage().contains("Gender")) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(createWrongGenderViolationMessage().toString())
                    .build();
        }
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity("\"" + exception.getMessage() + "\"")
                .build();
    }

    private static boolean hasExceptionInChain(Throwable throwable, Class<? extends Throwable> exceptionClass) {
        while (throwable != null) {
            if (exceptionClass.isInstance(throwable)) {
                return true;
            }
            throwable = throwable.getCause();
        }
        return false;
    }

    private JsonObject createWrongGenderViolationMessage() {
        JsonObject returnMessage = new JsonObject();
        returnMessage.addProperty("status", "400");
        returnMessage.addProperty("title", "Constraint Violation");
        JsonArray violations = new JsonArray();
        JsonObject violationDescription = new JsonObject();
        violationDescription.addProperty("field", "create.dog.gender");
        violationDescription.addProperty("message", "Gender should be MALE or FEMALE");
        violations.add(violationDescription);
        returnMessage.add("violations", violations);
        return returnMessage;
    }

}
