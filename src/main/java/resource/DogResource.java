package resource;


import entity.Dog;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.jboss.resteasy.reactive.ResponseStatus;
import service.DogService;

import java.util.List;


@Path("/api/v1/dogs")
public class DogResource {


    private final DogService dogService;

    @Inject
    public DogResource(DogService dogService) {
        this.dogService = dogService;
    }

    @GET
    public Uni<List<Dog>> get() {
        return dogService.list();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/find")
    public Uni<List<Dog>> getByName(@QueryParam("name") String name,
                                    @QueryParam("sortOrder") @DefaultValue("asc") String sortOrder) {
        return dogService.findAllDogsByName(name, sortOrder);
    }

    @POST
    @ResponseStatus(201)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Uni<Dog> create(@Valid Dog dog) {
        return dogService.create(dog);
    }

    @GET
    @Path("{id}")
    public Uni<Dog> get(@PathParam("id") long id) {
        return dogService.findById(id);
    }

    @PUT
    @Path("{id}")
    public Uni<Dog> update(@PathParam("id") long id, @Valid Dog dog) {
        dog.id = id;
        return dogService.update(dog);
    }

    @DELETE
    @Path("{id}")
    public Uni<Void> delete(@PathParam("id") long id) {
        return dogService.delete(id);
    }
}
