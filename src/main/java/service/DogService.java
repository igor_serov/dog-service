package service;


import entity.Dog;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import org.hibernate.ObjectNotFoundException;

import java.util.List;

@ApplicationScoped
public class DogService {

    private static final int MAX_DOGS = 10;
    private final Object lock = new Object();

    public Uni<Dog> findById(final long id) {
        return Dog.<Dog>findById(id).onItem().ifNull().failWith(() -> new ObjectNotFoundException(id, "id "));
    }

    public Uni<List<Dog>> findAllDogsByName(final String name, final String sortOrder) {
        Sort sort = "desc".equalsIgnoreCase(sortOrder) ? Sort.by("birthday")
                .descending() : Sort.by("birthday").ascending();

        return Dog.list("name", sort, name);
    }


    @WithTransaction
    public Uni<Dog> create(final Dog dog) {
        return Uni.createFrom().item(dog)
                .onItem().transformToUni(d -> {
                    synchronized (lock) {
                        return Dog.count()
                                .onItem().transformToUni(count -> {
                                    if (count < MAX_DOGS) {
                                        return dog.persistAndFlush();
                                    } else {
                                        return Uni.createFrom().failure(
                                                new WebApplicationException("Dog limit reached", Response.Status.BAD_REQUEST)
                                        );
                                    }
                                });
                    }
                });
    }

    public Uni<List<Dog>> list() {
        return Dog.listAll();
    }

    @WithTransaction
    public Uni<Dog> update(final Dog dog) {
        return findById(dog.id).chain(foundDog -> Dog.getSession()).chain(s -> s.merge(dog));
    }

    @WithTransaction
    public Uni<Void> delete(final long id) {
        return findById(id).chain(PanacheEntityBase::delete);
    }
}
