CREATE TABLE dogs
(
    id       SERIAL PRIMARY KEY,
    name     VARCHAR(50) NOT NULL CHECK (length(name) >= 3),
    gender   TEXT        NOT NULL CHECK (gender IN ('MALE', 'FEMALE')),
    birthday DATE        NOT NULL
);

CREATE SEQUENCE dogs_seq
    INCREMENT BY 1
    START WITH 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 10;


