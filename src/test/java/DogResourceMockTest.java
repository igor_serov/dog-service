import entity.Dog;
import entity.Gender;
import io.smallrye.mutiny.Uni;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import resource.DogResource;
import service.DogService;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DogResourceMockTest {

    @Mock
    private DogService dogService;

    @InjectMocks
    private DogResource dogResource;

    @Test
    public void getAllDogsTest() {
        when(dogService.list()).thenReturn(Uni.createFrom().item(List.of(new Dog())));

        Uni<List<Dog>> result = dogResource.get();
        result.subscribe().with(dogs -> {
            assertNotNull(dogs);
            assertFalse(dogs.isEmpty());
        });
    }

    @Test
    public void getDogByNameTest() {
        Dog testDog = new Dog();
        testDog.setName("Rex");
        testDog.setGender(Gender.MALE);
        testDog.setBirthday(LocalDate.of(2020, 1, 1));
        testDog.setId(1L);
        when(dogService.findAllDogsByName(eq("Rex"), eq("asc"))).thenReturn(Uni.createFrom().item(Collections.singletonList(testDog)));

        List<Dog> dogs = dogResource.getByName("Rex", "asc").await().indefinitely();

        assertNotNull(dogs);
        assertFalse(dogs.isEmpty());
        assertEquals("Rex", dogs.get(0).getName());

        verify(dogService).findAllDogsByName(eq("Rex"), eq("asc"));
    }

    @Test
    public void createDogTest() {

        Dog testDog = new Dog();
        testDog.setName("Rex");
        testDog.setGender(Gender.MALE);
        testDog.setBirthday(LocalDate.of(2020, 1, 1));
        testDog.setId(1L);
        when(dogService.create(any(Dog.class))).thenReturn(Uni.createFrom().item(testDog));

        Dog createdDog = dogResource.create(testDog).await().indefinitely();

        assertNotNull(createdDog);
        assertEquals(testDog.getName(), createdDog.getName());

        verify(dogService).create(any(Dog.class));
    }

    @Test
    void updateDogTest() {
        long dogId = 1L;
        Dog updatedDog = new Dog();
        updatedDog.setId(dogId);
        updatedDog.setName("Rex");
        updatedDog.setGender(Gender.MALE);
        updatedDog.setBirthday(LocalDate.of(2019, 1, 1));

        when(dogService.update(any(Dog.class))).thenReturn(Uni.createFrom().item(updatedDog));

        Dog result = dogResource.update(dogId, updatedDog).await().indefinitely();

        assertNotNull(result);
        assertEquals(dogId, result.getId());
        assertEquals("Rex", result.getName());

        verify(dogService).update(any(Dog.class));
    }

    @Test
    void deleteDogTest() {
        long dogId = 1L;
        when(dogService.delete(dogId)).thenReturn(Uni.createFrom().nullItem());
        assertDoesNotThrow(() ->
                dogResource.delete(dogId).await().indefinitely()
        );
        verify(dogService).delete(dogId);
    }
}
