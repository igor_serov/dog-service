package resource;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class DogResourceTest {

    @Test
    public void createDogTest() {
        given()
                .contentType(ContentType.JSON)
                .body("{\"name\":\"Mumu\",\"gender\":\"MALE\",\"birthday\":\"2022-01-01\"}")
                .when().post("/api/v1/dogs")
                .then()
                .statusCode(201);

    }

    @Test
    public void findDogByNameTest() {
        String name = given()
                .contentType(ContentType.JSON)
                .body("{\"name\":\"Mumu\",\"gender\":\"MALE\",\"birthday\":\"2022-01-01\"}")
                .when().post("/api/v1/dogs")
                .then()
                .statusCode(201)
                .extract()
                .path("name");

       given()
                .when().get("/api/v1/dogs?name=" + name)
                .then()
                .statusCode(200);


    }


    @Test
    public void findDogByIdTest() {
        int dogId = given()
                .contentType(ContentType.JSON)
                .body("{\"name\":\"Mumu\",\"gender\":\"MALE\",\"birthday\":\"2022-01-01\"}")
                .when().post("/api/v1/dogs")
                .then()
                .statusCode(201)
                .extract()
                .path("id");

        String resultName = given()
                .when().get("/api/v1/dogs/"+dogId)
                .then()
                .statusCode(200)
                .extract()
                .path("name");

        assertEquals("Mumu", resultName);

    }


    @Test
    public void deleteDogTest() {
        int dogId = given()
                .contentType(ContentType.JSON)
                .body("{\"name\":\"Mumu\",\"gender\":\"MALE\",\"birthday\":\"2022-01-01\"}")
                .when().post("/api/v1/dogs")
                .then()
                .statusCode(201)
                .extract()
                .path("id");

        given()
                .when().delete("/api/v1/dogs/" + dogId)
                .then()
                .statusCode(204);

    }


}
